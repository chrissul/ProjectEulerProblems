import java.awt.*;
import java.math.BigInteger;

public class Euler002{

     public static void main(String []args){
        int i, j;
        int MAX = 100;
        BigInteger[] number = new BigInteger[MAX];
        BigInteger sum = new BigInteger("0");

        for (i = 0; i < MAX; i++) {
            if (i == 0 || i ==1) {
                if (i == 0) {
                    number[0] = new BigInteger("1");
                } else {
                    number [1] = new BigInteger("2");
                }
            } else {
                number[i] = number[i-1].add(number[i-2]);
            }
        }

        // for(BigInteger x : number){
        //     System.out.print(x + " ");
        // }

        i = 0;
        BigInteger max = new BigInteger("4000000");
        BigInteger two = new BigInteger("2");
        while (number[i].compareTo(max) == -1) {
            if (number[i].mod(two).compareTo(new BigInteger("0")) == 0) {
                sum = sum.add(number[i]);
            }
            i++;
        }

        System.out.println(sum);
     }
}
